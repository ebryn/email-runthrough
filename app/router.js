import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('messages', function() {
    this.route('folder', { path: ':folder_id' }, function() {
      this.route('email', { path: 'email/:email_id' });
    });
    
  });
});

export default Router;
