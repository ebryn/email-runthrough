import Ember from 'ember';

export default Ember.Route.extend({
  model({folder_id}) {
    return this.store.findRecord('folder', folder_id, {include: 'emails'});
  }
});
